/*
 Navicat Premium Data Transfer

 Source Server         : 本地连接
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : dsmserver

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/09/2019 17:16:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '操作人id',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作方法',
  `method_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法描述',
  `request_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人ip地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '监控中心', NULL, NULL, 0, 'fa fa-tachometer', 1);
INSERT INTO `sys_menu` VALUES (2, 0, '运营中心', NULL, NULL, 0, 'fa fa-desktop', 2);
INSERT INTO `sys_menu` VALUES (3, 0, '客户中心', NULL, NULL, 0, 'fa fa-users', 3);
INSERT INTO `sys_menu` VALUES (4, 0, '系统设置', NULL, NULL, 0, 'fa fa-cog', 4);
INSERT INTO `sys_menu` VALUES (6, 0, '实时监控', NULL, NULL, 0, 'fa fa-eye', 6);
INSERT INTO `sys_menu` VALUES (7, 0, '智能分析', NULL, NULL, 0, 'fa fa-cog', 7);
INSERT INTO `sys_menu` VALUES (8, 1, '大屏展示', 'sys/dashboard.vm', NULL, 1, NULL, 1);
INSERT INTO `sys_menu` VALUES (9, 2, '设备管理', 'sys/device.vm', NULL, 1, NULL, 1);
INSERT INTO `sys_menu` VALUES (10, 3, '服务商管理', 'sys/provider.vm', NULL, 1, NULL, 1);
INSERT INTO `sys_menu` VALUES (11, 3, '企业管理', 'sys/company.vm', NULL, 1, NULL, 2);
INSERT INTO `sys_menu` VALUES (12, 4, '账号管理', 'sys/user.html', '', 1, '', 1);
INSERT INTO `sys_menu` VALUES (13, 4, '角色管理', 'sys/role.html', NULL, 1, NULL, 2);
INSERT INTO `sys_menu` VALUES (14, 4, '菜单管理', 'sys/menu.html', NULL, 1, NULL, 3);
INSERT INTO `sys_menu` VALUES (15, 4, 'SQL监控', 'druid/sql.html', NULL, 1, NULL, 4);
INSERT INTO `sys_menu` VALUES (16, 9, '查看', NULL, 'sys:device:info,sys:device:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 9, '新增', NULL, 'sys:device:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 9, '删除', NULL, 'sys:device:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 9, '修改', NULL, 'sys:device:update,sys:device:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 10, '查看', NULL, 'sys:provider:info,sys:provider:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 10, '新增', NULL, 'sys:provider:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 10, '删除', NULL, 'sys:provider:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 10, '修改', NULL, 'sys:provider:update,sys:provider:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 11, '查看', NULL, 'sys:company:info,sys:company:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 11, '新增', NULL, 'sys:company:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 11, '删除', NULL, 'sys:company:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 11, '修改', NULL, 'sys:company:update,sys:company:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (28, 12, '查看', NULL, 'sys:user:info,sys:user:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (29, 12, '新增', NULL, 'sys:user:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (30, 12, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (31, 12, '修改', NULL, 'sys:user:update,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (32, 13, '查看', NULL, 'sys:role:info,sys:role:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (33, 13, '新增', NULL, 'sys:role:save,sys:menu:perms', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (34, 13, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (35, 13, '修改', NULL, 'sys:role:update,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (36, 14, '查看', NULL, 'sys:menu:info,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (37, 14, '新增', NULL, 'sys:menu:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (38, 14, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (39, 14, '修改', NULL, 'sys:menu:update,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (40, 6, '实时监控', 'sys/monitor.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (41, 7, '智能分析', 'sys/intelligent.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (42, 1, '地图监控', 'sys/map.vm', '', 1, '', 2);
INSERT INTO `sys_menu` VALUES (43, 0, '历史记录', NULL, NULL, 0, 'fa fa-bell', 8);
INSERT INTO `sys_menu` VALUES (44, 43, '报警日志', 'sys/alarmlog.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (45, 56, '设备运行数据', 'sys/operation.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (49, 0, '测试添加', NULL, NULL, 0, '', 0);
INSERT INTO `sys_menu` VALUES (50, 2, '设备检修', 'sys/maintenance.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (51, 6, '设备报修', 'sys/repairs.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (52, 51, '新增', NULL, 'sys:repairs:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (54, 51, '修改', NULL, 'sys:repairs:update', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (55, 50, '修改', NULL, 'sys:maintenance:update', 2, NULL, NULL);
INSERT INTO `sys_menu` VALUES (56, 0, '统计分析', NULL, NULL, 0, 'fa fa-file-excel-o', 9);
INSERT INTO `sys_menu` VALUES (57, 56, '数据统计', 'sys/analyze.vm', '', 1, '', 0);
INSERT INTO `sys_menu` VALUES (58, 4, '系统公告', 'sys/notifies.vm', '', 1, '', 5);
INSERT INTO `sys_menu` VALUES (59, 58, '新增', NULL, 'sys:notifies:save', 2, NULL, NULL);
INSERT INTO `sys_menu` VALUES (60, 58, '查看', NULL, 'sys:notifies:list,sys:notifies:info', 2, NULL, NULL);
INSERT INTO `sys_menu` VALUES (61, 58, '修改', NULL, 'sys:notifies:update', 2, NULL, NULL);
INSERT INTO `sys_menu` VALUES (62, 58, '删除', NULL, 'sys:notifies:delete', 2, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '所属父类id',
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` int(11) NULL DEFAULT NULL COMMENT '创建人账号id',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', '2019-05-05 13:53:57', 1);
INSERT INTO `sys_role` VALUES (2, '运维管理员', '运维管理员全部权限', '2019-05-05 13:53:57', 1);
INSERT INTO `sys_role` VALUES (3, '服务商角色', '服务商全部权限', '2019-05-05 13:53:57', 1);
INSERT INTO `sys_role` VALUES (4, '企业角色', '企业全部权限', '2019-05-05 13:53:57', 1);
INSERT INTO `sys_role` VALUES (9, '次级管理员', 'fws添加', '2019-06-25 10:41:10', 10);
INSERT INTO `sys_role` VALUES (10, '设备管理人员', 'fws添加', '2019-07-22 08:49:59', 10);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2745 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1641, 9, 1);
INSERT INTO `sys_role_menu` VALUES (1642, 9, 8);
INSERT INTO `sys_role_menu` VALUES (1643, 9, 42);
INSERT INTO `sys_role_menu` VALUES (1644, 9, 2);
INSERT INTO `sys_role_menu` VALUES (1645, 9, 9);
INSERT INTO `sys_role_menu` VALUES (1646, 9, 16);
INSERT INTO `sys_role_menu` VALUES (1647, 9, 3);
INSERT INTO `sys_role_menu` VALUES (1648, 9, 11);
INSERT INTO `sys_role_menu` VALUES (1649, 9, 24);
INSERT INTO `sys_role_menu` VALUES (1650, 9, 25);
INSERT INTO `sys_role_menu` VALUES (1651, 9, 26);
INSERT INTO `sys_role_menu` VALUES (1652, 9, 27);
INSERT INTO `sys_role_menu` VALUES (1653, 9, 4);
INSERT INTO `sys_role_menu` VALUES (1654, 9, 12);
INSERT INTO `sys_role_menu` VALUES (1655, 9, 28);
INSERT INTO `sys_role_menu` VALUES (1656, 9, 29);
INSERT INTO `sys_role_menu` VALUES (1657, 9, 30);
INSERT INTO `sys_role_menu` VALUES (1658, 9, 31);
INSERT INTO `sys_role_menu` VALUES (1659, 9, 13);
INSERT INTO `sys_role_menu` VALUES (1660, 9, 32);
INSERT INTO `sys_role_menu` VALUES (1661, 9, 33);
INSERT INTO `sys_role_menu` VALUES (1662, 9, 34);
INSERT INTO `sys_role_menu` VALUES (1663, 9, 35);
INSERT INTO `sys_role_menu` VALUES (1936, 10, 2);
INSERT INTO `sys_role_menu` VALUES (1937, 10, 9);
INSERT INTO `sys_role_menu` VALUES (1938, 10, 16);
INSERT INTO `sys_role_menu` VALUES (2067, 4, 1);
INSERT INTO `sys_role_menu` VALUES (2068, 4, 42);
INSERT INTO `sys_role_menu` VALUES (2069, 4, 2);
INSERT INTO `sys_role_menu` VALUES (2070, 4, 9);
INSERT INTO `sys_role_menu` VALUES (2071, 4, 16);
INSERT INTO `sys_role_menu` VALUES (2072, 4, 4);
INSERT INTO `sys_role_menu` VALUES (2073, 4, 12);
INSERT INTO `sys_role_menu` VALUES (2074, 4, 28);
INSERT INTO `sys_role_menu` VALUES (2075, 4, 29);
INSERT INTO `sys_role_menu` VALUES (2076, 4, 30);
INSERT INTO `sys_role_menu` VALUES (2077, 4, 31);
INSERT INTO `sys_role_menu` VALUES (2078, 4, 13);
INSERT INTO `sys_role_menu` VALUES (2079, 4, 32);
INSERT INTO `sys_role_menu` VALUES (2080, 4, 33);
INSERT INTO `sys_role_menu` VALUES (2081, 4, 34);
INSERT INTO `sys_role_menu` VALUES (2082, 4, 35);
INSERT INTO `sys_role_menu` VALUES (2083, 4, 6);
INSERT INTO `sys_role_menu` VALUES (2084, 4, 40);
INSERT INTO `sys_role_menu` VALUES (2085, 4, 51);
INSERT INTO `sys_role_menu` VALUES (2086, 4, 52);
INSERT INTO `sys_role_menu` VALUES (2087, 4, 54);
INSERT INTO `sys_role_menu` VALUES (2088, 4, 7);
INSERT INTO `sys_role_menu` VALUES (2089, 4, 41);
INSERT INTO `sys_role_menu` VALUES (2090, 4, 43);
INSERT INTO `sys_role_menu` VALUES (2091, 4, 44);
INSERT INTO `sys_role_menu` VALUES (2092, 4, 45);
INSERT INTO `sys_role_menu` VALUES (2448, 2, 1);
INSERT INTO `sys_role_menu` VALUES (2449, 2, 8);
INSERT INTO `sys_role_menu` VALUES (2450, 2, 2);
INSERT INTO `sys_role_menu` VALUES (2451, 2, 9);
INSERT INTO `sys_role_menu` VALUES (2452, 2, 16);
INSERT INTO `sys_role_menu` VALUES (2453, 2, 17);
INSERT INTO `sys_role_menu` VALUES (2454, 2, 18);
INSERT INTO `sys_role_menu` VALUES (2455, 2, 19);
INSERT INTO `sys_role_menu` VALUES (2456, 2, 3);
INSERT INTO `sys_role_menu` VALUES (2457, 2, 10);
INSERT INTO `sys_role_menu` VALUES (2458, 2, 20);
INSERT INTO `sys_role_menu` VALUES (2459, 2, 11);
INSERT INTO `sys_role_menu` VALUES (2460, 2, 24);
INSERT INTO `sys_role_menu` VALUES (2461, 2, 4);
INSERT INTO `sys_role_menu` VALUES (2462, 2, 12);
INSERT INTO `sys_role_menu` VALUES (2463, 2, 28);
INSERT INTO `sys_role_menu` VALUES (2464, 2, 13);
INSERT INTO `sys_role_menu` VALUES (2465, 2, 32);
INSERT INTO `sys_role_menu` VALUES (2466, 2, 33);
INSERT INTO `sys_role_menu` VALUES (2467, 2, 34);
INSERT INTO `sys_role_menu` VALUES (2468, 2, 35);
INSERT INTO `sys_role_menu` VALUES (2469, 2, 14);
INSERT INTO `sys_role_menu` VALUES (2470, 2, 36);
INSERT INTO `sys_role_menu` VALUES (2471, 2, 37);
INSERT INTO `sys_role_menu` VALUES (2472, 2, 38);
INSERT INTO `sys_role_menu` VALUES (2473, 2, 39);
INSERT INTO `sys_role_menu` VALUES (2474, 2, 15);
INSERT INTO `sys_role_menu` VALUES (2654, 3, 1);
INSERT INTO `sys_role_menu` VALUES (2655, 3, 8);
INSERT INTO `sys_role_menu` VALUES (2656, 3, 42);
INSERT INTO `sys_role_menu` VALUES (2657, 3, 2);
INSERT INTO `sys_role_menu` VALUES (2658, 3, 9);
INSERT INTO `sys_role_menu` VALUES (2659, 3, 16);
INSERT INTO `sys_role_menu` VALUES (2660, 3, 17);
INSERT INTO `sys_role_menu` VALUES (2661, 3, 18);
INSERT INTO `sys_role_menu` VALUES (2662, 3, 19);
INSERT INTO `sys_role_menu` VALUES (2663, 3, 50);
INSERT INTO `sys_role_menu` VALUES (2664, 3, 55);
INSERT INTO `sys_role_menu` VALUES (2665, 3, 3);
INSERT INTO `sys_role_menu` VALUES (2666, 3, 11);
INSERT INTO `sys_role_menu` VALUES (2667, 3, 24);
INSERT INTO `sys_role_menu` VALUES (2668, 3, 25);
INSERT INTO `sys_role_menu` VALUES (2669, 3, 26);
INSERT INTO `sys_role_menu` VALUES (2670, 3, 27);
INSERT INTO `sys_role_menu` VALUES (2671, 3, 4);
INSERT INTO `sys_role_menu` VALUES (2672, 3, 12);
INSERT INTO `sys_role_menu` VALUES (2673, 3, 28);
INSERT INTO `sys_role_menu` VALUES (2674, 3, 29);
INSERT INTO `sys_role_menu` VALUES (2675, 3, 30);
INSERT INTO `sys_role_menu` VALUES (2676, 3, 31);
INSERT INTO `sys_role_menu` VALUES (2677, 3, 13);
INSERT INTO `sys_role_menu` VALUES (2678, 3, 32);
INSERT INTO `sys_role_menu` VALUES (2679, 3, 33);
INSERT INTO `sys_role_menu` VALUES (2680, 3, 34);
INSERT INTO `sys_role_menu` VALUES (2681, 3, 35);
INSERT INTO `sys_role_menu` VALUES (2682, 3, 6);
INSERT INTO `sys_role_menu` VALUES (2683, 3, 51);
INSERT INTO `sys_role_menu` VALUES (2684, 3, 52);
INSERT INTO `sys_role_menu` VALUES (2685, 3, 54);
INSERT INTO `sys_role_menu` VALUES (2686, 3, 43);
INSERT INTO `sys_role_menu` VALUES (2687, 3, 44);
INSERT INTO `sys_role_menu` VALUES (2688, 3, 49);
INSERT INTO `sys_role_menu` VALUES (2689, 3, 56);
INSERT INTO `sys_role_menu` VALUES (2690, 3, 45);
INSERT INTO `sys_role_menu` VALUES (2691, 3, 57);
INSERT INTO `sys_role_menu` VALUES (2692, 1, 1);
INSERT INTO `sys_role_menu` VALUES (2693, 1, 8);
INSERT INTO `sys_role_menu` VALUES (2694, 1, 42);
INSERT INTO `sys_role_menu` VALUES (2695, 1, 2);
INSERT INTO `sys_role_menu` VALUES (2696, 1, 9);
INSERT INTO `sys_role_menu` VALUES (2697, 1, 16);
INSERT INTO `sys_role_menu` VALUES (2698, 1, 17);
INSERT INTO `sys_role_menu` VALUES (2699, 1, 18);
INSERT INTO `sys_role_menu` VALUES (2700, 1, 19);
INSERT INTO `sys_role_menu` VALUES (2701, 1, 50);
INSERT INTO `sys_role_menu` VALUES (2702, 1, 3);
INSERT INTO `sys_role_menu` VALUES (2703, 1, 10);
INSERT INTO `sys_role_menu` VALUES (2704, 1, 20);
INSERT INTO `sys_role_menu` VALUES (2705, 1, 21);
INSERT INTO `sys_role_menu` VALUES (2706, 1, 22);
INSERT INTO `sys_role_menu` VALUES (2707, 1, 23);
INSERT INTO `sys_role_menu` VALUES (2708, 1, 11);
INSERT INTO `sys_role_menu` VALUES (2709, 1, 24);
INSERT INTO `sys_role_menu` VALUES (2710, 1, 25);
INSERT INTO `sys_role_menu` VALUES (2711, 1, 26);
INSERT INTO `sys_role_menu` VALUES (2712, 1, 27);
INSERT INTO `sys_role_menu` VALUES (2713, 1, 4);
INSERT INTO `sys_role_menu` VALUES (2714, 1, 12);
INSERT INTO `sys_role_menu` VALUES (2715, 1, 28);
INSERT INTO `sys_role_menu` VALUES (2716, 1, 29);
INSERT INTO `sys_role_menu` VALUES (2717, 1, 30);
INSERT INTO `sys_role_menu` VALUES (2718, 1, 31);
INSERT INTO `sys_role_menu` VALUES (2719, 1, 13);
INSERT INTO `sys_role_menu` VALUES (2720, 1, 32);
INSERT INTO `sys_role_menu` VALUES (2721, 1, 33);
INSERT INTO `sys_role_menu` VALUES (2722, 1, 34);
INSERT INTO `sys_role_menu` VALUES (2723, 1, 35);
INSERT INTO `sys_role_menu` VALUES (2724, 1, 14);
INSERT INTO `sys_role_menu` VALUES (2725, 1, 36);
INSERT INTO `sys_role_menu` VALUES (2726, 1, 37);
INSERT INTO `sys_role_menu` VALUES (2727, 1, 38);
INSERT INTO `sys_role_menu` VALUES (2728, 1, 39);
INSERT INTO `sys_role_menu` VALUES (2729, 1, 15);
INSERT INTO `sys_role_menu` VALUES (2730, 1, 58);
INSERT INTO `sys_role_menu` VALUES (2731, 1, 59);
INSERT INTO `sys_role_menu` VALUES (2732, 1, 60);
INSERT INTO `sys_role_menu` VALUES (2733, 1, 61);
INSERT INTO `sys_role_menu` VALUES (2734, 1, 62);
INSERT INTO `sys_role_menu` VALUES (2735, 1, 6);
INSERT INTO `sys_role_menu` VALUES (2736, 1, 40);
INSERT INTO `sys_role_menu` VALUES (2737, 1, 51);
INSERT INTO `sys_role_menu` VALUES (2738, 1, 52);
INSERT INTO `sys_role_menu` VALUES (2739, 1, 54);
INSERT INTO `sys_role_menu` VALUES (2740, 1, 43);
INSERT INTO `sys_role_menu` VALUES (2741, 1, 44);
INSERT INTO `sys_role_menu` VALUES (2742, 1, 56);
INSERT INTO `sys_role_menu` VALUES (2743, 1, 45);
INSERT INTO `sys_role_menu` VALUES (2744, 1, 57);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `job` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `type` int(1) NULL DEFAULT NULL COMMENT '用户类型1：运营商，2服务商，3企业',
  `provider_id` bigint(20) NULL DEFAULT NULL COMMENT '所属服务商ID',
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '所属企业ID',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_delete` int(1) NULL DEFAULT 1 COMMENT '是否删除，0表示删除，1表示不删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '8a3c9086d9c8c9f24fe5fdd88a7fefc4', '超级管理员', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-05 13:53:57', 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);


SET FOREIGN_KEY_CHECKS = 1;
