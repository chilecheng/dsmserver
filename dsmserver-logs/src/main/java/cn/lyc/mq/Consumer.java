package cn.lyc.mq;

import cn.lyc.entity.SysLog;
import cn.lyc.util.JDBCUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName Consumer
 * @Description {TODO}
 * @Author wh
 * #Date 2019-08-30 14:14
 */
@Component
public class Consumer {

//    @Autowired
//    private SysLogMapper mapper;
    @JmsListener(destination = "logsQueue")
    public void receive(String sysLog) {
        JSONObject jsonObject = JSONObject.parseObject(sysLog);
        SysLog sysLog1 = JSON.parseObject(jsonObject.toJSONString(), new TypeReference<SysLog>() {
        });
//        mapper.save(sysLog1);
        JDBCUtils.add(sysLog1);
    }
}
