package cn.lyc.entity;


import lombok.Data;
import org.springframework.stereotype.Component;
import java.util.Date;

@Data
public class SysUser {


    private Long id;

    private String username;

    private String password;

    private String nickname;

    private String phone;

    private String email;

    private String job;

    private String address;

    private Integer type;

    private Long provider_id;

    private Long company_id;

    private String provider;

    private String company;

    private Long createUser;

    private Date create_time;

    private Integer is_delete;

    private Integer role_id;


}
