package cn.lyc.util;

import cn.lyc.entity.SysLog;
import cn.lyc.entity.SysUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * @ClassName JDBCUtils
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-10 15:58
 */
public class JDBCUtils {

    private static String driver;

    private static String url ;

    private static String user ;

    private static String password ;

    private JDBCUtils(){}

    static {
        /**
         * 驱动注册
         */
        try {
            YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
            ClassPathResource classPathResource = new ClassPathResource("application.yml");
            yaml.setResources(classPathResource != null?classPathResource:new ClassPathResource("bootstrap.yml"));
            Properties properties=yaml.getObject();
            driver = properties.get("spring.datasource.driver-class-name").toString();
            url = properties.get("spring.datasource.url").toString();
            user = properties.get("spring.datasource.username").toString();
            password = properties.get("spring.datasource.password").toString();
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new ExceptionInInitializerError(e);
        }

    }

    /**
     * 获取 Connetion
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException{
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * 释放资源
     * @param conn
     * @param st
     * @param rs
     */
    public static void colseResource(Connection conn, Statement st, ResultSet rs) {
        closeResultSet(rs);
        closeStatement(st);
        closeConnection(conn);
    }

    /**
     * 释放连接 Connection
     * @param conn
     */
    public static void closeConnection(Connection conn) {
        if(conn !=null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //等待垃圾回收
        conn = null;
    }

    /**
     * 释放语句执行者 Statement
     * @param st
     */
    public static void closeStatement(Statement st) {
        if(st !=null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //等待垃圾回收
        st = null;
    }

    /**
     * 释放结果集 ResultSet
     * @param rs
     */
    public static void closeResultSet(ResultSet rs) {
        if(rs !=null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //等待垃圾回收
        rs = null;
    }

    public static void add(SysLog sysLog) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            // 获取连接
            conn = JDBCUtils.getConnection();

            // 编写sql
            String sql = "insert into sys_log(user_id,method_name,method_description,request_ip,create_time) values (?,?,?,?,?)";

            // 创建语句执行者
            st= conn.prepareStatement(sql);

            //设置参数
            st.setLong(1, sysLog.getUser_id());
            st.setString(2, sysLog.getMethod_name());
            st.setString(3, sysLog.getMethod_description());
            st.setString(4, sysLog.getRequest_ip());
            st.setTimestamp(5, new Timestamp(sysLog.getCreate_time().getTime()));
//            System.out.println(sysLog.getCreate_time().getTime());
            // 执行sql
            int i = st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.colseResource(conn, st, rs);
        }

    }
    public static SysUser QueryObjectByName(String username){
        List<SysUser> list = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet result = null;
        try {
            //创建连接
            conn = JDBCUtils.getConnection();
            //创建prepareStatement对象，用于执行SQL
            ps = conn.prepareStatement("select * from sys_user where username=?");
            ps.setString(1,username);
            //获取查询结果集
            result = ps.executeQuery();
            while(result.next()){
                SysUser sysUser = new SysUser();
                sysUser.setUsername(result.getString("username"));
                sysUser.setId(result.getLong("id"));
                sysUser.setNickname(result.getString("nickname"));
                list.add(sysUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.colseResource(conn, ps, result);
            return list.get(0)!=null?list.get(0):null;
        }
    }
}
