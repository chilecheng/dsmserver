package cn.lyc.util;

import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.UUID;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import sun.misc.BASE64Decoder;



public class JWTUtils {

    private static String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApp7Lp9Xrlgb62abp2GuvjkX/xS6bc2hlyVooVrPjB1TQ++MlbYLKN8ShxUuoKq92nwe7NlOwmV4o9Doob3/XHm+f/b3aw1qghW4koKIT4LiDQVCnEKnsXBEI8VyfK5mm6ZLoxEledpfD0rtUNERJK/CsLv04NWtUcgRXvjmBNzXa1IKR9J0GiaQ1kn4nh5doMSQ0sl/D8iKFuD5ww4rpxI6Tx8SePD6QOY/BGKkuEgPDX/5LfpmDo3rAH+qfvg2+HVAmPjUkvvKP8LPt8k7Q8d034yPnbIfzI4HEBDt3Fh3iAVqTWROdK6XGBg66JojledZjeCxbWj0tgygjQl8RPQIDAQAB";
    private static String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmnsun1euWBvrZpunYa6+ORf/FLptzaGXJWihWs+MHVND74yVtgso3xKHFS6gqr3afB7s2U7CZXij0Oihvf9ceb5/9vdrDWqCFbiSgohPguINBUKcQqexcEQjxXJ8rmabpkujESV52l8PSu1Q0REkr8Kwu/Tg1a1RyBFe+OYE3NdrUgpH0nQaJpDWSfieHl2gxJDSyX8PyIoW4PnDDiunEjpPHxJ48PpA5j8EYqS4SA8Nf/kt+mYOjesAf6p++Db4dUCY+NSS+8o/ws+3yTtDx3TfjI+dsh/MjgcQEO3cWHeIBWpNZE50rpcYGDromiOV51mN4LFtaPS2DKCNCXxE9AgMBAAECggEAPggajAahab55f7SblN4qgs5etbtvVl0EoBz3rafVGvKhx1U+pvGtdWu4SmqrVhDzGavYMBaiRcb/ekV4rmIq5YLrTp1zJ43C+0oRSthsjBpFi3iKajLI81HIQYuZGrNep9Dw5brm90HzPq5NYBQYJcq5X9kYJxMpYjVpsUFZcQL5ic8AErM7E4z5MB2jP1Okj4TLNUxwEd00MPXbdMDwjOnDHxnnZ1Trr5mfVVAE1fi7MXsNlmKCDDrDFzsqIeGzPngPH1mU05P17HUnsq8WCn6N4UnXq44Qf2Fq6UQUDAGKXmroK2RrEz89o3DCNCBqL/3vdDyroRytknp+2/QGBQKBgQDtZCQXvgKEDD8BYlAaLF52UQQT2vcscxWBCsDVEcXfc+mVR3eEgMDh5ctMeKjFr/dnKlKsewf5FUwGuPBbizwzhLAgzxaN4QMwkQrMMJrf00gyyGlslu5gN5b73uMMVwxTsxiwOK/fNxaKYLx/Vzd2tumSEeD0hhbv1vbVq7JV0wKBgQCzrnTZG7LCdpiI3A4UGmxucAXTvlipJByeoOZDivG76HluFmcdr3wDNDQap6AxQcSqP6gK5qai9oz+W3cGlm78jPGtEx2/gx3Tr6nCf9W9gIgG0PiQE7TKS33j7sEgTZbC51i+XnyqqeIJ9TnUGkH0DOztv29sRBZ5cfsgzZJCrwKBgDCws35zKIzw/hNImOllZFKcN1vjM4NYb3ayP+7z4EP+wFAwAmGZDvbGbTZYITW66+GrGp3p9i6uoZNXZ2U/X6+pbX6tOxEKr97MQQy0g2+ZJFvWI1l0c5wkisFKbANh71NTjqLGXbxIzkS/pqnvt56P0cR2Ck/dGONEkggtWTylAoGBALIxV0xgvb1BJDDGG2gwdExp6D1/zimf2Iowg2uEhguWl6ZnrUexWslKokm055cx5Dn6+0okSCnhXR570uTDl7n6hUrhZGUjQRdIZJCxemV+7GfmIjuwb2EX0PeVNg41JEiSal4REo2mALdjbUA98h54GxgE/BlgxPEX6rczZ9OdAoGAdwFPf7UugEZQSMcBaNh7q26ievmc+mgSfjh5DYd2EPuBj3qMlK5JklOu1+iq0CKNazKKwW0PWUQdVvf92uHTHj6+tpGhq9euct2MMqdXcn2qXyNLLFI3rMLei5ND3upvNAXRjyHCDicSXPwCNDXJjyoVg2XvfN/ejzS/f5bL7W8=";

    /**
     * 生成JWT
     * @param username
     * @return
     * @throws Exception
     */
    public  String signRs256(String username) throws Exception {
        Date now = new Date();

        Date expires = new Date(now.getTime() + 1000*60*60*24*7 /* 7 day */);
        String jwt = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setId(UUID.randomUUID().toString())
                .setSubject(username)
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(expires)
                .signWith(SignatureAlgorithm.RS256, (RSAPrivateKey) getPrivateKey(privateKey))
                .compact();
        return jwt;
    }

    /**
     * 验证JWT
     * @param jwt
     * @return
     */
    public   boolean verify(String jwt){
        try {
            Jwts.parser().setSigningKey((Key) getPublicKey(publicKey)).parseClaimsJws(jwt);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     }
     }

     /**
     * 获取用户名
     * @param jwt
     * @return
     * @throws Exception
     */
    public static String getUserName(String jwt) throws Exception {
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey((Key) getPublicKey(publicKey))
                .parseClaimsJws(jwt);
        String subject = claimsJws.getBody().getSubject();
        return subject;
    }



    /**
     * 将String转化为公钥
     * @param key
     * @return
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return (RSAPublicKey)publicKey;
    }

    /**
     * 将String转化为私钥
     * @param key
     * @return
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return (RSAPrivateKey)privateKey;
    }


}
