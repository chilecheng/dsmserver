package cn.lyc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName LogsApplication
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-11 10:11
 */
@SpringBootApplication
@EnableEurekaClient
//@ComponentScan({"cn.lyc.*"})
@MapperScan("cn.lyc.dao")
public class LogsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogsApplication.class);
    }
}
