package cn.lyc.api.service.impl;


import cn.lyc.api.service.SysRoleMenuService;
import cn.lyc.api.service.SysRoleService;
import cn.lyc.dao.SysRoleMapper;
import cn.lyc.entity.SysRole;
import cn.lyc.utils.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
@CacheConfig(cacheNames = "SysRoleInfo",keyGenerator = "myKeyGenerator")
public class SysRoleServiceImpl implements SysRoleService {
	
    @Autowired
    private SysRoleMapper sysRoleMapper;
    
    @Autowired
    private SysRoleMenuService sysRoleMenuService;
    
    @RequestMapping("/queryRolesByUser")
    @Override
    @Cacheable
    public List<SysRole> queryRolesByUser(@RequestBody Map<String, Object> map) {
        return sysRoleMapper.queryRolesByUser(map);
    }
    
    @RequestMapping("/queryRolesByUserCount")
    @Override
    @Cacheable
    public int queryRolesByUserCount(@RequestBody Map<String, Object> map) {
        return sysRoleMapper.queryRolesByUserCount(map);
    }
    
    @RequestMapping("/saveSysRole")
    @Override
    public void save(@RequestBody SysRole role) {
        role.setCreate_time(new Date());
        System.out.println(role.toString());
        sysRoleMapper.save(role);

        //保存角色与菜单关系
        sysRoleMenuService.saveOrUpdate(role.getRole_id(), role.getMenuIdList());
    }
    
    @RequestMapping("/queryObjectSysRole")
    @Override
//    @Cacheable
    public SysRole queryObject(@RequestParam("role_id") Long role_id) {
        return sysRoleMapper.queryObject(role_id);
    }

    @RequestMapping("/updateSysRole")
    @Override
//    @Caching(
//            evict = {
//                    @CacheEvict(key = "'SysRoleServiceImpl.queryObject['+#role.role_id+']'"),
//            }
//    )
    public void updateSysRole(@RequestBody SysRole role) {
        sysRoleMapper.updateSysRole(role);
        System.out.println(role.getRole_id());
        //更新角色与菜单关系
        sysRoleMenuService.saveOrUpdate(role.getRole_id(), role.getMenuIdList());
    }
    
    @RequestMapping("/deleteSysRole")
    @Override
    public void deleteSysRole(@RequestParam("role_id") Long role_id) {
        if(role_id==1||role_id==2||role_id==3||role_id==4){
            throw new ServiceException("系统级角色不可删除");
        }
        sysRoleMapper.deleteSysRole(role_id);
    }
    

}
