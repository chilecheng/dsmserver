package cn.lyc.api.service.impl;

import cn.lyc.api.service.SysMenuService;
import cn.lyc.dao.SysMenuMapper;
import cn.lyc.dao.SysUserMapper;
import cn.lyc.entity.SysMenu;
import cn.lyc.utils.MenuType;
import cn.lyc.utils.ServiceException;
import cn.lyc.vo.MenuVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;


@Slf4j
@Service
@CacheConfig(cacheNames = "SysMenuInfo",keyGenerator = "myKeyGenerator")
public class SysMenuServiceImpl implements SysMenuService {
	
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisTemplate redisTemplate;


    @RequestMapping("/getUserMenuList")
    @Override
    @Cacheable
    public List<SysMenu> getUserMenuList(Long userId) {
        //超级管理员不是拥有全部菜单，而是数据库指定的菜单
        /*if(userId == 1){
            return getAllMenuList(null);
        }*/

        //用户菜单列表
        List<Long> menuIdList = sysUserMapper.queryAllMenuId(userId);
        return getAllMenuList(menuIdList);
    }

    @Override
    @RequestMapping("/getUserMenuVOList")
    public List<MenuVO> getUserMenuVOList(Long userId) {
        List<Long> menuIdList = sysUserMapper.queryAllMenuId(userId);
        List<SysMenu> allMenuList = getAllMenuList(menuIdList);

        List <MenuVO> menuVOlist = new ArrayList<>();

        allMenuList.forEach(menu -> {
            MenuVO menuVO = menuToMenuVO(menu);
            List<SysMenu> list = (List<SysMenu>) menu.getList();
            List <MenuVO> children = new ArrayList<>();
            list.forEach(menu1 -> {
                MenuVO menuVO1 = menuToMenuVO(menu1);
                menuVO1.setChildren(new ArrayList<>());
                children.add(menuVO1);
            });
            menuVO.setChildren(children);

            menuVOlist.add(menuVO);
        });

        return menuVOlist;
    }

    private MenuVO menuToMenuVO(SysMenu menu){
        MenuVO menuVO = new MenuVO();
        menuVO.setPath(menu.getUrl());
        menuVO.setName(menu.getName());
        menuVO.setRedirect(menu.getRedirect());
        menuVO.setComponent(menu.getComponent());
        HashMap<String, String> map = new HashMap<>();
        map.put("title", menu.getMenuname());
        map.put("icon", menu.getIcon());
        menuVO.setMeta(map);
        return menuVO;
    }

    /**
     * 获取所有菜单列表
     */
    private List<SysMenu> getAllMenuList(List<Long> menuIdList){
        //查询根菜单列表
        List<SysMenu> menuList = queryListParentId(0L, menuIdList);
        //递归获取子菜单
        getMenuTreeList(menuList, menuIdList);

        return menuList;
    }
    
    @RequestMapping("/queryListParentId")
    @Override
    @Cacheable
    public List<SysMenu> queryListParentId(Long parentId, List<Long> menuIdList) {
        List<SysMenu> menuList = sysMenuMapper.queryListParentId(parentId);
        if(menuIdList == null){
            return menuList;
        }

        List<SysMenu> userMenuList = new ArrayList<>();
        for(SysMenu menu : menuList){
            if(menuIdList.contains(menu.getMenu_id())){
                userMenuList.add(menu);
            }
        }
        return userMenuList;
    }
    
    /**
     * 递归
     */
    private List<SysMenu> getMenuTreeList(List<SysMenu> menuList, List<Long> menuIdList){
        List<SysMenu> subMenuList = new ArrayList<>();

        for(SysMenu entity : menuList){
            if(entity.getType() == MenuType.CATALOG.getValue()){//目录
                entity.setList(getMenuTreeList(queryListParentId(entity.getMenu_id(), menuIdList), menuIdList));
            }
            subMenuList.add(entity);
        }

        return subMenuList;
    }
    
    /**
     * 查询菜单列表
     */
    @RequestMapping("/queryList")
    @Override
    @Cacheable
    public List<SysMenu> queryList(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        return sysMenuMapper.queryList(map);
    }
    
    /**
     * 查询菜单列表（不带分页）总数
     * @return
     */
    @RequestMapping("/queryTotal")
    @Override
    @Cacheable
    public int queryTotal() {
        return sysMenuMapper.queryTotal();
    }
    
    @RequestMapping("/save")
    @Override
    public void save(@RequestBody SysMenu menu) {
        verifyForm(menu);
        int row = sysMenuMapper.insertSelective(menu);
        if(row==0){
            throw new ServiceException("保存失败");
        }
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysMenu menu){
        if(StringUtils.isBlank(menu.getName())){
            throw new ServiceException("菜单名称不能为空");
        }

        if(menu.getParent_id() == null){
            throw new ServiceException("上级菜单不能为空");
        }

        //菜单
        if(menu.getType() == MenuType.MENU.getValue()){
            if(StringUtils.isBlank(menu.getUrl())){
                throw new ServiceException("菜单URL不能为空");
            }
        }

        //上级菜单类型
        int parentType = MenuType.CATALOG.getValue();
        if(menu.getParent_id() != 0){
            SysMenu parentMenu = queryObject(menu.getParent_id());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if(menu.getType() == MenuType.CATALOG.getValue() ||
                menu.getType() == MenuType.MENU.getValue()){
            if(parentType != MenuType.CATALOG.getValue()){
                throw new ServiceException("上级菜单只能为目录类型");
            }
            return ;
        }

        //按钮
        if(menu.getType() == MenuType.BUTTON.getValue()){
            if(parentType != MenuType.MENU.getValue()){
                throw new ServiceException("上级菜单只能为菜单类型");
            }
            return ;
        }
    }

    @RequestMapping("/queryObject")
    @Override
    @Cacheable
    public SysMenu queryObject(@RequestParam("menuId") Long menuId) {
        return sysMenuMapper.queryObject(menuId);
    }

    @RequestMapping("/update")
	@Override
    @CacheEvict(key = "'SysMenuServiceImpl.queryObject['+#sysMenu.menu_id+']'")
	public void update(@RequestBody SysMenu sysMenu) {
    	verifyForm(sysMenu);
        int row = sysMenuMapper.updateByPrimaryKeySelective(sysMenu);
        if(row==0){
            throw new ServiceException("更新失败");
        }
	}

    @RequestMapping("/deleteBatch")
    @Override
   // @CacheRemove(key = "SysMenuInfo::SysMenuServiceImpl.queryTotal")
    @CacheEvict(key = "'SysMenuServiceImpl.queryTotal[]'")
    public void deleteBatch(@RequestBody Long[] menuIds) {
        System.out.println(menuIds);
    	for (Long long1 : menuIds) {
    		int row = sysMenuMapper.deleteBatch(long1);
    		if(row==0){
                throw new ServiceException("删除失败");
            }
//            if(row!=menuIds.length){
//                throw new ServiceException("部分菜单无法删除");
//            }
		}
    	//批量删除redis中的缓存
        Set<String> keys = redisTemplate.keys("SysMenuInfo::SysMenuServiceImpl" + "*");
        System.out.println(keys);
        redisTemplate.delete(keys);
        
    }




 
}
