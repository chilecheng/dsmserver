package cn.lyc.api.service;

import cn.lyc.entity.SysJob;
import cn.lyc.utils.TaskException;
import org.quartz.SchedulerException;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ll moonlight
 * @date: 2019-09-04 14:38
 */
public interface SysJobService {

    public List<SysJob> selectAllJob(SysJob job);
    public List<SysJob> getJobList(Map<String, Object> map);
    public int queryJobCount();

    int changeStatus( SysJob job) throws SchedulerException;
    public int pauseJob(SysJob job) throws SchedulerException;

    public int resumeJob(SysJob job) throws SchedulerException;

    public void insertJob(SysJob job) throws SchedulerException, TaskException;

    public void updateJob(SysJob job) throws SchedulerException, TaskException;

    public boolean checkCronExpressionIsValid(String cronExpression);

    public SysJob selectJobById(Long jobId);

    public void deleteJob(SysJob job) throws SchedulerException;

    public void run(SysJob job) throws SchedulerException;
}
