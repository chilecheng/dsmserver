package cn.lyc.api.service;

import cn.lyc.entity.SysRole;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

//@RequestMapping(name = "user")
public interface SysRoleService {
	
    /**
     * 根据账户查找其对应的公司或服务商已创建的角色
     * @param map
     * @return
     */
    List<SysRole> queryRolesByUser(Map<String,Object> map);
	
    /**
     * 根据账户查找其对应的公司或服务商已创建的角色数量
     * @param map
     * @return
     */
    int queryRolesByUserCount(Map<String, Object> map);
	
    /**
     * 保存
     * @param role
     */
    void save(@RequestBody SysRole role);
	
    /**
     * 查询单个角色对象
     * @param role_id
     * @return
     */
    SysRole queryObject(@RequestParam("role_id") Long role_id);
	
	/**
     * 更新角色
     * @param role
     */
    void updateSysRole(@RequestBody SysRole role);
	
	
    /**
     * 删除
     * @param role_id
     */
    void deleteSysRole(@RequestParam("role_id") Long role_id);
	

	
}
