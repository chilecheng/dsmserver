package cn.lyc.api.service.impl;


import cn.lyc.api.service.SysUserRoleService;
import cn.lyc.api.service.SysUserService;
import cn.lyc.dao.SysUserMapper;
import cn.lyc.entity.SysUser;
import cn.lyc.utils.RoleUtil;
import cn.lyc.utils.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@CacheConfig(cacheNames = "UserInfo",keyGenerator = "myKeyGenerator")
@Service
public class UserServiceImpl implements SysUserService {

	@Autowired
	private SysUserMapper userDao;

	@Autowired
	private SysUserRoleService sysUserRoleService;

	//根据用户名查询
	@RequestMapping("/login")
	@Override
	@Cacheable
	public SysUser query_userName(@RequestParam("username") String username) {
		System.out.println(username);
		SysUser user =  userDao.query_userName(username);
		return user;
	}
	
	//防止删除数据库中admin
	@RequestMapping("/addAdmin")
	@Override
	public void addAdmin(@RequestBody SysUser users) {
		userDao.addAdmin(users);
	}

	@RequestMapping("/queryAllPerms")
    @Override
    public List<String> queryAllPerms(Long userId) {
        return userDao.queryAllPerms(userId);
    }
	
	@RequestMapping("/queryUsers")
    @Override
    public List<SysUser> queryUsers(@RequestBody Map<String, Object> map) {
        return userDao.queryUsers(map);
    }
	
	@RequestMapping("/queryUsersCount")
    @Override
    public int queryUsersCount(@RequestBody Map<String, Object> map) {
        return userDao.queryUsersCount(map);
    }
	
	@RequestMapping("/updateUser")
    @Override
    public int updateUser(@RequestBody SysUser user) {
//        SysUser sysUser = ShiroUtil.getUserEntity();
        SysUser queryUser = userDao.query_userName(user.getUsername());
        if(queryUser != null && (queryUser.getId() != user.getId())){
            throw new ServiceException("用户名被占用");
        }
//        if (user.getPassword() != null && user.getPassword() != "") {
//            user.setPassword(user.getPassword());
//        }
        return userDao.updateByPrimaryKeySelective(user);
    }

	@RequestMapping("/addUser")
	@Override
	public void addUser(@RequestBody SysUser user,int type) {
		String username = user.getUsername();
		if(username == null || StringUtils.isBlank(username)){
			throw new ServiceException("用户名不能为空");
		}
		if(userDao.query_userName(username)!=null){
			throw new ServiceException("用户名被占用");
		}
//		SimpleHash sh = new SimpleHash("MD5","000000", ByteSource.Util.bytes(username),1024);
//		user.setPassword(sh.toString());
//		user.setCreate_user(ShiroUtil.getUserEntity().getId());
		ByteSource credentialsSalt = ByteSource.Util.bytes(username);
		String md5Hex = DigestUtils.md5Hex(credentialsSalt+"000000");
		user.setPassword(md5Hex);
//		user.setCreate_user();

		user.setCreate_time(new Date());
		int row = userDao.insertSelective(user);
		if(row!=1){
			throw new ServiceException("账号添加失败");
		}
		//开始绑定权限
		List<Long> role = new ArrayList<>();
		int rtype = RoleUtil.getRoleType(user);
		role.add(new Long(rtype+1));
//        role.add(Long.valueOf(user.getrole_id()));
		sysUserRoleService.saveUserRole(user.getId(), role);
	}

	@Override
	@RequestMapping("/userQueryList")
	public List<SysUser> queryList(@RequestBody Map<String, Object> map) {
		return userDao.queryList(map);
	}

	@Override
	@RequestMapping("/userQueryListCount")
	public int queryListCount(@RequestBody Map<String, Object> map) {
		return userDao.queryListCount(map);
	}

	@Override
	@RequestMapping("/userRoleQueryList")
	public List<SysUser> queryRoleList(@RequestBody Map<String, Object> map) {
		return userDao.queryRoleList(map);
	}

	@Override
	@RequestMapping("/userRoleQueryListCount")
	public int queryRoleListCount(@RequestBody Map<String, Object> map) {
		return userDao.queryRoleListCount(map);
	}


	@RequestMapping("/test")
	public String jwtaa(HttpServletRequest request){
		String authorization = request.getHeader("Authorization");
		return authorization;
	}
}
