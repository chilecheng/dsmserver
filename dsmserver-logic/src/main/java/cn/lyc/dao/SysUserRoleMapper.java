package cn.lyc.dao;



import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.lyc.entity.SysRole;

public interface SysUserRoleMapper {
    /**
     * 保存用户与角色的关系
     * @param map
     * @return
     */
    int save(Map<String, Object> map);

    /**
     * 删除用户与角色关系
     * @param id
     * @return
     */
    int delete(@Param("id") Object id);

    /**
     * 查询id包含的角色
     * @param id
     * @return
     */
    List<SysRole> queryHasRole(@Param("id") long id);
}
