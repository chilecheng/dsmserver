package cn.lyc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.lyc.entity.SysUser;


public interface SysUserMapper {
	
	//登录(根据用户名)
//	@Select("select * from sys_user WHERE username=#{username}")
	public SysUser query_userName(@Param("username") String username);
	
	//防止删除数据库中admin
	public void addAdmin(SysUser users);
	
	/**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(@Param("userId") Long userId);
	
    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(@Param("userId") Long userId);
	
	List<SysUser> queryUsers(Map<String,Object> map);
	
	int queryUsersCount(Map<String,Object> map);
	
    /**
     * 选择性的更新
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(SysUser record);

	/**
	 * 选择性插入
	 * @param record
	 * @return
	 */
	int insertSelective(SysUser record);


	List<SysUser> queryList(Map<String,Object> map);
	int queryListCount(Map<String,Object> map);


	List<SysUser> queryRoleList(Map<String,Object> map);
	int queryRoleListCount(Map<String,Object> map);
}
