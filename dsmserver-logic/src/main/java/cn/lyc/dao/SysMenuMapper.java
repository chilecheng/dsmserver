package cn.lyc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.RequestBody;

import cn.lyc.entity.SysMenu;

public interface SysMenuMapper {
	
	/**
     * 根据父菜单，查询子菜单
     * @param parentId 父菜单ID
     */
	public List<SysMenu> queryListParentId(@Param("parentId") Long parentId);
	
	/**
     * 查询菜单列表
     */
	
	public List<SysMenu> queryList(Map<String, Object> map);
	
	/**
     * 查询菜单列表（不带分页）总数
     * @return
     */
	public int queryTotal();
	
	SysMenu queryObject(@Param("menuId") Long menuId);
	
	int insertSelective(SysMenu menu);

	public int updateByPrimaryKeySelective(SysMenu sysMenu);

	public int deleteBatch( Long menuIds);
	
	

}
