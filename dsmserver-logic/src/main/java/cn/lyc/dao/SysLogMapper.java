package cn.lyc.dao;


import cn.lyc.entity.SysLog;

import java.util.List;
import java.util.Map;

/**
 *
 */
//@Mapper
public interface SysLogMapper {

    /**
     * 按条件查询操作
     *
     * @param map
     * @return
     */
    List<SysLog> queryList(Map<String, Object> map);

    /**
     * 添加操作日志记录
     *
     * @param sysLog
     */
    void save(SysLog sysLog);

    /**
     * 按条件查询日志条数
     * @param map
     * @return
     */
    int queryListTotal(Map<String,Object> map);
}

