package cn.lyc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统页面视图
 * 
 */
@Controller
public class SysPageController {
	
	@RequestMapping("/{url}.html")
	public String page1(@PathVariable("url") String url){
		return  url ;
	}
	@RequestMapping("/{parent}/{url}.html")
	public String page1(@PathVariable("parent") String parent,@PathVariable("url") String url){
		return parent+"/" + url ;
	}
}
