package cn.lyc.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysMenu {
    private Long menu_id;

    private Long parent_id;

    private String parentName;

    private String name;

    private String url;

    private String perms;

    private Integer type;

    private String icon;

    private Integer order_num;

    private Boolean open;

    private String component;

    private String redirect;

    private String menuname;

    private List<?> list;


    @Override
    public String toString() {
        return "SysMenu{" +
                "menu_id=" + menu_id +
                ", parent_id=" + parent_id +
                ", parentName='" + parentName + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", perms='" + perms + '\'' +
                ", type=" + type +
                ", icon='" + icon + '\'' +
                ", order_num=" + order_num +
                ", open=" + open +
                ", list=" + list +
                '}';
    }
}

