package cn.lyc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRole implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    private Long role_id;

    /**
     * 角色名称
     */
    private String role_name;

    /**
     * 备注
     */
    private String remark;

    private List<Long> menuIdList;

    /**
     * 创建时间
     */
    private Date create_time;

    private Integer parent_id;

   private Long create_user;


    @Override
    public String toString() {
        return "SysRole{" +
                "role_id=" + role_id +
                ", role_name='" + role_name + '\'' +
                ", remark='" + remark + '\'' +
                ", menuIdList=" + menuIdList +
                ", create_time=" + create_time +
                ", parent_id=" + parent_id +
              ", create_user=" + create_user +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj instanceof SysRole){
            SysRole role = (SysRole) obj;
            return role.getRole_id() == this.getRole_id() && role.getRole_name().equals(this.getRole_name()) && role.getCreate_user() == this.getCreate_user();
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (role_id == null ? 0 : role_id.hashCode());
        result = 31 * result + (role_name == null ? 0 : role_name.hashCode());
        result = 31 * result + (create_user == null ? 0 : create_user.hashCode());
        return result;
    }
}

